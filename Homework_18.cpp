#include <iostream>
#include <vector>
#include <string>

template<typename T>
class Stack
{
public:
	void pop()
	{
		v.pop_back();
	}

	void push(T a)
	{
		v.push_back(a);
	}

	void show()
	{
		for (const auto& elements : v)
		{
			std::cout << elements << "\n";
		}
	}

private:
	std::vector<T> v;
};

int main()
{
	std::cout << "Type int:\n\n";
	Stack<int> v1;
	v1.push(1);
	v1.push(2);
	v1.push(3);
	v1.show();
	std::cout << "============\n";
	v1.pop();
	v1.show();
	std::cout << "\n////////////\n";
	std::cout << "////////////\n\n";

	std::cout << "Type string:\n\n";
	Stack<std::string> v2;
	v2.push("Vladimirov");
	v2.push("Daniil");
	v2.push("ROmanovich");
	v2.show();
	std::cout << "============\n";
	v2.pop();
	v2.show();
	std::cout << "\n////////////\n";
	std::cout << "////////////\n\n";

	std::cout << "Type float:\n\n";
	Stack<float> v3;
	v3.push(5.1);
	v3.push(15.563);
	v3.push(36.6);
	v3.show();
	std::cout << "============\n";
	v3.pop();
	v3.show();
	std::cout << "\n////////////\n";
	std::cout << "////////////\n\n";

	std::cout << "Type double:\n\n";
	Stack<double> v4;
	v4.push(10.1875);
	v4.push(100.176);
	v4.push(36.6);
	v4.show();
	std::cout << "============\n";
	v4.pop();
	v4.show();
	return 0;
}


